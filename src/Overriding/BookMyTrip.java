package Overriding;

public class BookMyTrip extends Travel
{
    @Override
    void ticket(int seat)
    {
        double total=seat*price;
        double finalAmt=total-total*0.05;
        System.out.println("Final Amount for your ticket from BookMytrip is: "+finalAmt);
    }
}
