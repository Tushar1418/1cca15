package Overriding;

public class Amazon extends Ecommerce
{
    @Override
    void sellProduct(int qty, double price)
    {
        double total=qty*price;
        double finalAmt=total-total*0.05;
        System.out.println("Total Final Amount is:"+finalAmt);
    }
}
