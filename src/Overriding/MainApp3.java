package Overriding;

import java.util.Scanner;

public class MainApp3  
{
    public static void main(String[] args)
    {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter no. of Seats you want to book: ");
        int seat=sc1.nextInt();


        System.out.println("Select the Booking platform ");
        System.out.println("1:Redbus\n2:Bookmytrip");

        int choice=sc1.nextInt();
        if (choice==1)
        {
            Redbus r1=new Redbus();

            r1.ticket(seat);
        } else if (choice==2)
        {
            BookMyTrip b1=new BookMyTrip();
            b1.ticket(seat);
        }
        else
        {
            System.out.println("Invalid choice");
        }
    }
}
