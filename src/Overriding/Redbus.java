package Overriding;

public class Redbus extends Travel
{
    @Override
    void ticket(int seat)
    {
        double total=seat*price;
        double finalAmt=total-total*0.1;
        System.out.println("Final Amount from Redbus is"+finalAmt);
    }
}
