import java.util.Scanner;

public class ForLoopDemo6
{
    public static void main(String[] args)
    {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter total no. of user");
        int count= sc1.nextInt();
        for (int a=1;a<=count;a++)
        {
            System.out.println("Enter user name");
            String name= sc1.next();
            System.out.println("Welcome:"+name);
        }

    }
}
