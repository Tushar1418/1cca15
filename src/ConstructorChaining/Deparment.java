package ConstructorChaining;

public class Deparment extends College
{
    Deparment(String universityName,String collegeName,String deparmentName)
    {
        super(collegeName,universityName);
        System.out.println("DEPARMENT :"+deparmentName);
    }
}
