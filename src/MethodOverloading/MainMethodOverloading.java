package MethodOverloading;

public class MainMethodOverloading
{   // Standard Method
    public static void main(String[] args)
    {
        System.out.println("Main Method");
        main(25);
        main('T');
    }

    //External or Non Standard method

    public static void main (int a)
    {
        System.out.println("A:"+a);
        System.out.println("Main Method2");
    }
    public static void main(char c)
    {
        System.out.println("c:"+c);
        System.out.println("Main method3");
    }
}
