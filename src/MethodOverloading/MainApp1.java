package MethodOverloading;

import java.util.Scanner;

public class MainApp1
{
    public static void main(String[] args)
    {
        Student s1=new Student();
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Serach Criteria");
        System.out.println("1:Search by Name\n2:Search by Contact");

        int choice= sc1.nextInt();
        if (choice==1)
        {
            System.out.println("Enter the Name");
            String name= sc1.next();
            s1.Search(name);
        } else if (choice==2)
        {
            System.out.println("Eneter the Contact no.");
            int Contact= sc1.nextInt();
            s1.Search(Contact);
        }
        else
        {
            System.out.println("Invalid choice");
        }
    }
}
