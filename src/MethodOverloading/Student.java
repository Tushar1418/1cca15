package MethodOverloading;

public class Student
{
    String name="Tushar";
    int contact=12345;

    void Search(String studentName)
    {
        if (studentName.equalsIgnoreCase(name))
        {
            System.out.println("Student Name is"+name);
            System.out.println("Student contact number is"+contact);
        }

        else
        {
            System.out.println("Record not found");
        }
    }

    void Search(int contactNo)
    {
        if (contactNo==contact)
        {
            System.out.println("Student name is"+name);
            System.out.println("Student contact number is"+contact);
        }

        else
        {
            System.out.println("Record not Found");
        }
    }
}
