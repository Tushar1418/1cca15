package AbstractClass;

import java.util.Scanner;

public class MainApp4
{
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("1:Manager\n2:Watchman");
        int choice= sc1.nextInt();
        Employee e1=null;
        if (choice==1)
        {
            e1 = new Manager();
        } else if (choice==2)
        {
            e1=new WatchMan();
        }

        e1.getDesignation();
        e1.getsalary();

    }
}
