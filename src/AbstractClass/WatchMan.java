package AbstractClass;

public class WatchMan extends Employee
{
    @Override
    void getDesignation()
    {
        System.out.println("Designation is Watchman");
    }

    @Override
    void getsalary()
    {
        System.out.println("Salary is 25000");
    }
}
