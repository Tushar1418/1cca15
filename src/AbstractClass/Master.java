package AbstractClass;

public abstract class Master
{
    //SUPERCLASS
    void test()
    {
        System.out.println("Test Method");
    }

    abstract void Display();

}
