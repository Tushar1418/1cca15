package AbstractClass;

public  class Manager extends Employee
{
    @Override
    void getDesignation()
    {
        System.out.println("Designation is Manager");
    }

    @Override
    void getsalary()
    {
        System.out.println("Salary is 2500000");
    }
}
