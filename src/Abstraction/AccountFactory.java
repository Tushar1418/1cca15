package Abstraction;
//Factory Class
public class AccountFactory
{
    //Factory Method

    Account createAccount(int type,double balance)
    {
        Account a1=null;
        if (type==1)
        {
            a1=new SavingsAccount(balance);//upcasting
        }
        else if (type==2)
        {
            a1=new LoanAccount(balance);//upcating
        }
        return a1;
    }

}
