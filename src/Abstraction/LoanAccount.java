package Abstraction;

public class LoanAccount implements Account
{
  private  double loanAmount;
    //Account creation

    public LoanAccount(double loanAmount)
    {
        this.loanAmount=loanAmount;
        System.out.println("Loan Account Created");
    }

    @Override
    public void deposit(double amt)
    {
        loanAmount-=amt;
        System.out.println(amt+" Rs Debited from Loan Account");
    }

    @Override
    public void withDraw(double amt)
    {
        loanAmount+=amt;
        System.out.println(amt+" Rs Credited to your Account");
    }

    @Override
    public void checkbal()
    {
        System.out.println("Active loan Aamount "+loanAmount);
    }
}
