package Abstraction;

public class SavingsAccount implements Account{
   private double accountBalance;

    public SavingsAccount(double accountBalance)
    {
        this.accountBalance=accountBalance;
        System.out.println("Savings Account created");
    }
    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+" Rs credited to your account");

    }

    @Override
    public void withDraw(double amt)
    {
        if(amt<=accountBalance)
        {
            accountBalance-=amt;
            System.out.println(amt+" Rs debited");
        }
        else
        {
            System.out.println("Insuffient balance");
        }

    }

    @Override
    public void checkbal()
    {
        System.out.println("Active Balance"+accountBalance);

    }
}
