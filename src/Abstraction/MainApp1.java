package Abstraction;

import java.util.Scanner;

//Utilizaton Layer
public class MainApp1
{
    public static void main(String[] args)
    {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println("1:Savings\n2:Loan");
        int accType= sc1.nextInt();
        System.out.println("Enter Account Opening Balance");
        double balance= sc1.nextDouble();

        AccountFactory factory=new AccountFactory();
        Account accRef=factory.createAccount(accType,balance);
        boolean status=true;

        while (status)
        {
            System.out.println("Select mode of Trasnsction");
            System.out.println("1:Deposit\n2:Withdraw\n3:Check Balance\n4:Exit");
            int choice= sc1.nextInt();
            if (choice==1)
            {
                System.out.println("Enter Amount");
                double amt= sc1.nextDouble();
                accRef.deposit(amt);
            } else if (choice==2)
            {
                System.out.println("Enter Amount");
                double amt= sc1.nextDouble();
                accRef.withDraw(amt);
            } else if (choice==3)
            {
                accRef.checkbal();
            }
            else
            {
                status=false;
            }
        }

    }
}
