package Arr;

public class StoreManager {
    static String[]products={"TV","Projector","Mobile"};
    static double[]cost={15000,10000,14000};
    static int[]stock={25,10,50};

    void CalculateBill(int choice,int qty)
    {
        boolean found=false;
        for(int a=0;a< products.length;a++)
        {
            if(choice==a &&  qty<=stock[a])
            {
                double total=qty*cost[a];
                stock[a]-=qty;
                System.out.println("Total Bill"+total);
                found=true;
            }
        }

        if (!found)
        {
            System.out.println("Product not found or out of stock");
        }
    }


}
