package Arr;

public class BillCalculator {
    public double[] calculateBill(double[] amount) {
        double[] gstValues = gstCalculation(amount);

        //create new array to calculate the bill amount

        double[] totalAmount = new double[amount.length];

        //Perform sum of amounts and gst values

        for (int a = 0; a < amount.length; a++) {
            totalAmount[a] = amount[a] + gstValues[a];
        }
        double totalBillAmt = 0.0;
        double totalGstAmt = 0.0;
        double totalFinalAmt = 0.0;

        //Sum of Array elements

        for (int a = 0; a < amount.length; a++) {
            totalBillAmt += amount[a];
            totalGstAmt += gstValues[a];
            totalFinalAmt += totalAmount[a];
        }

        //present final amount to user

        System.out.println("Bill.amt\tGSTAmt\tTotal");
        System.out.println("===========================================");

        for (int a = 0; a < amount.length; a++) {
            System.out.println(amount[a] + "\t" + gstValues[a] + "\t" + totalAmount[a]);

        }
        System.out.println("========================");
        System.out.println(totalBillAmt + "\t" + totalGstAmt + "\t" + totalFinalAmt);

        return gstValues;
    }

    public double[] gstCalculation(double[] amounts) {

        //create new array to store the gstValues

        double[] gstAmounts = new double[amounts.length];
        for (int a = 0; a < amounts.length; a++) {
            if (amounts[a] < 500) {
                gstAmounts[a] = amounts[a] * 0.05;
            } else {
                gstAmounts[a] = amounts[a] * 0.01;
            }
        }
        return gstAmounts;


    }
}






