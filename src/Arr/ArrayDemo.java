package Arr;

import java.util.Scanner;

public class ArrayDemo {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BillCalculator b1=new BillCalculator();
        System.out.println("Enter total no. of bills");
        int count= sc1.nextInt();
        double[] amount=new double[count];
        System.out.println("Enter Bill Amount");

        //Accept Bill Amount
        for(int a=0;a<count;a++){
            amount[a]=sc1.nextDouble();
        }

        //call
        b1.calculateBill(amount);

    }
}
