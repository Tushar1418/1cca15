package Arr;

import java.util.Scanner;

public class ArrayDemo8 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while (status)
        {
            System.out.println("Select product");
            System.out.println("0:TV\n1:Projector\n2:Mobile\n3:exit");
            int choice= sc1.nextInt();

            StoreManager sc=new StoreManager();
            if (choice==0 || choice==1 || choice==2)
            {
                System.out.println("Enter qty");
                int qty=sc1.nextInt();
                sc.CalculateBill(choice,qty);
            } else if (choice==3) {
                System.exit(0);

            } else {
                System.out.println("Invalid choice");
                status=false;
            }
        }
    }
}
