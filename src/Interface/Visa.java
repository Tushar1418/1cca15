package Interface;

public class Visa implements CreditCard
{
    @Override
    public void getType() {
        System.out.println("Credit Card type is Visa");
    }

    @Override
    public void Withdraw(double amt) {
        System.out.println("Transcation is successfull of RS "+amt);
    }
}
