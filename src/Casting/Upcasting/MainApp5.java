package Casting.Upcasting;

public class MainApp5
{
    public static void main(String[] args) {
        Master m1=new Sample();
        m1.test();


        Sample s1=(Sample) new Master();
        s1.display();
        s1.test();
    }
}
/* Master m1=new Master();
if(m1 instanceOf Sample)
{
  Sample s1=(Sample)m1;
  }
  else
  {
    sop("Properties not found")
 */