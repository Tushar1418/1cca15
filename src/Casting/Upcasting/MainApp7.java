package Casting.Upcasting;

public class MainApp7
{
    public static void main(String[] args) {
        Bike b1=new ElectricBike();
        b1.getType();
        ElectricBike e=(ElectricBike) b1;
        e.batteryInfo();
    }
}
