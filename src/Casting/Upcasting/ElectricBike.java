package Casting.Upcasting;

public class ElectricBike extends Bike
{
    @Override
    void getType()
    {
        System.out.println("Bike Type is Electric");
    }

    void batteryInfo()
    {
        System.out.println("Battery size is 100 volt");
    }
}
