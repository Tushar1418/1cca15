package Casting;

public class CastingDemo4
{
    public static void main(String[] args)
    {
        short s1= (short) 123456;
        System.out.println(s1);

        long l1=894563214l;
        int x1=(int) l1;
        System.out.println(x1);
    }
}
