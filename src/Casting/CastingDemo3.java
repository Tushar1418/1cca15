package Casting;

public class CastingDemo3
{
    public static void main(String[] args)
    {
        char ch1='T';
        char ch2='U';

        int x1=ch1;
        int x2=ch2;
        System.out.println(x1+"\t\t"+x2);

        int x3=97;
        char ch3= (char) x3;
        double x4=65.0;
        char ch4= (char) x4;
        System.out.println(ch3+"\t\t"+ch4);

    }
}
