package Casting.ClasstypeCasting;

public class AirAsia extends Goibibo
{
    double[]cost={7000.35,9000.25,11000.55};

    @Override
    void bookTickets(int qty, int routeChoice)
    {
        boolean found=false;
        for (int a=0;a<routes.length;a++)
        {
            if (routeChoice==a)
            {
                double total=qty*cost[a];
                System.out.println("Total Amt:"+total);
                found=true;
            }
        }
        if (!found)
        {
            System.out.println("Invalid choice");
        }
    }
}
