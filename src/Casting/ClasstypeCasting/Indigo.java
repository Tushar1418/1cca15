package Casting.ClasstypeCasting;

public class Indigo extends Goibibo
{
    double[]cost={3000,5000.5,8000.54};

    @Override
    void bookTickets(int qty, int routeChoice)
    {
        boolean found=false;
        for (int a=0;a<routes.length;a++)
        {
            if (routeChoice==a)
            {
                double total=qty*cost[a];
                System.out.println("Total Amt:"+total);
                found=true;
            }
        }
        if (!found)
        {
            System.out.println("Invalid choice");
        }
    }
}
