package Casting.ClasstypeCasting;

import java.util.Scanner;

public class MainApp3
{
    public static void main(String[] args)
    {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter the Quantity");
        int qty= sc1.nextInt();
        System.out.println("Enter the Price");
        double price= sc1.nextDouble();
        System.out.println("Select the Type of Machine");
        System.out.println("1:Laptop\n2:Projector");
        int choice= sc1.nextInt();
        Machine m1=null;

        if (choice==1)
        {
            m1=new Laptop();//Upcasting
        }
        else if (choice==2)
        {
            m1=new Projector();//Upcasting
        }
        else
        {
            System.out.println("Invalid choice");
        }
        m1.CalculateBill(qty, price);
        m1.getType();
    }
}
