package Casting.ClasstypeCasting;

import java.util.Scanner;

public class MainApp4
{
    public static void main(String[] args)
    {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Service Provider");
        System.out.println("1:AirAsia\n2:Inndigo");

        int choice= sc1.nextInt();
        System.out.println("Select the Route");
        System.out.println("0:Pune-Mumbai");
        System.out.println("1:Delhi-Chennai");
        System.out.println("2:Amalner-Shirpur");

        int routeChoice= sc1.nextInt();
        System.out.println("Enter the no. of Tickets");
        int tickets= sc1.nextInt();

        Goibibo g1=null;
        if (choice==1)
        {
            g1=new AirAsia();
        } else if (choice==2)
        {
            g1=new Indigo();
        }
        else
        {
            System.out.println("Invalid");
        }
        g1.bookTickets(tickets,routeChoice);
    }
}
