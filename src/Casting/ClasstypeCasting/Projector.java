package Casting.ClasstypeCasting;

public class Projector extends Machine
{
    @Override
    void getType()
    {
        System.out.println("Machine Type is Projector");
    }

    @Override
    void CalculateBill(int qty,double price)
    {
        double total=qty*price;
        double FinalAmt=total+total*0.5;

        System.out.println("Final Amount is:"+FinalAmt);
    }
}
